import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import { AccessAlarm, Phone } from "@mui/icons-material";
import React, { useEffect, useRef, useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
// import Peer from "simple-peer";
import io from "socket.io-client";
import "./App.css";

//客户端的socket连接
const socket = io.connect("http://localhost:5000");

function App() {
  //创建存储的状态
  const [stream, setStream] = useState(null);
  const [name, setName] = useState("");
  const [me, setMe] = useState("");
  const [idToCall, setIdToCall] = useState("");
  const [callAcceded, setCallAcceded] = useState(false); //初始通话状态
  const [callEnded, setCallEnded] = useState(false); //结束通话状态
  const [receivingCall, setReceivingCall] = useState(false); //维持通话的状态
  const [userName, setUserName] = useState("");
  const myVideo = useRef();

  useEffect(() => {
    navigator.mediaDevices
      .getUserMedia({ video: true, audio: true })
      .then((stream) => {
        setStream(stream);
        myVideo.current.srcObject = stream;
      })
      .catch((err) => {
        // alert(err);
        // window.location.href = `https://stackoverflow.com/search?q=js+${err}`;
      });
    socket.on("me", (id) => {
      setMe(id);
    });
  }, []);

  //向另一方拨打视频电话
  const callUser = (idToCall) => {};

  const answerCall = () => {};
  return (
    <>
      <h1 className="caller">视频聊天应用</h1>
      <main className="container">
        {/* 视频容器 */}
        <section className="video-container">
          <div className="video">
            {stream && (
              <video
                playsInline
                muted
                autoPlay
                style={{ width: "500px" }}
                ref={myVideo}
              />
            )}
          </div>
        </section>
        {/* 输入面板 */}
        <section className="myId">
          <TextField
            id="filled-basic"
            label="姓名"
            variant="filled"
            value={name}
            onChange={(e) => setName(e.target.value)}
            style={{ marginBottom: "20px" }}
          />
          <CopyToClipboard text={me} style={{ marginBottom: "2rem" }}>
            <Button
              variant="contained"
              color="primary"
              startIcon={<AccessAlarm fontSize="large" />}
            >
              我的通话ID
            </Button>
          </CopyToClipboard>
          <TextField
            id="filled-basic"
            label="请输入对方通话ID"
            variant="filled"
            value={idToCall}
            onChange={(e) => setIdToCall(e.target.value)}
          />
          <section className="call-button">
            {/* 接收到通信但又没有挂断的情况下，显示button为结束通信，否则就显示电话图标 */}
            {callAcceded && !callEnded ? (
              <Button variant="contained" color="secondary">
                结束通话
              </Button>
            ) : (
              <IconButton
                color="primary"
                aria-label="call"
                onClick={() => callUser(idToCall)}
              >
                <Phone fontSize="large" />
              </IconButton>
            )}
          </section>
        </section>
        {/* 同意接听 */}
        {receivingCall && !callAcceded ? (
          <section className="caller">
            <div>{userName}正在呼叫...</div>
            <Button variant="contained" color="primary" onClick={answerCall}>
              同意接听
            </Button>
          </section>
        ) : null}
      </main>
    </>
  );
}

export default App;
